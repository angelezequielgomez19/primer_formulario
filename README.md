# Proyecto Formulario   :rocket:

### Listado de tareas a desarrollar

- Apellidos
- Nombres
- Fecha de la asistencia al evento (el mismo será en 3 días corridos, puede seleccionar 1 como varios)
- Correo electrónico
- Fecha de nacimiento
- Redes sociales
- Perfil del inscripto
- Charla , meet o cursos en los cuales asistira

Estos datos deberán ser pasados al back, el cual los recibira y realizara algún tipo de validación antes de grabarlos en un archivo plano, manteniendo una estructura. No se podrán tener repetida la casilla de correo, en el caso que suceda se le deberá informar al usuario que ya existe la misma. En el caso de que sea satisfactoria la grabación de los datos, deberá mostrar un mensaje de exito.

### Equipo

- Angel Gomez
- Jeronimo Albusto