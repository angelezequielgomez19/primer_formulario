<?php

$errors = []; // creo un array para almacenar los errores

// Compruebo si se ha enviado el formulario 
if ($_SERVER["REQUEST_METHOD"] == "POST") {

  // Valido el nombre
  if (empty($_POST['name'])) {
    $errors[] = "El nombre es obligatorio.";
  } elseif (strlen($_POST['name']) > 10) {
    $errors[] = "El nombre no debe exceder los 10 caracteres.";
  }


  // Valido el apellido
  if (empty($_POST['surname'])) {
    $errors[] = "El apellido es obligatorio.";
  } elseif (strlen($_POST['surname']) > 10) {
    $errors[] = "El apellido no debe exceder los 10 caracteres.";
  }



  // Valido la fecha de nacimiento
  if (empty($_POST['date'])) {
    $errors[] = "La fecha de nacimiento es obligatoria.";
  } else {
    $date = $_POST['date']; 
    $current_date = date("Y-m-d"); 

    // Compruebo si la fecha de nacimiento es posterior o igual a la fecha actual
    if ($date >= $current_date) {
      $errors[] = "La fecha de nacimiento debe ser anterior a la fecha actual.";
    }


    // Calculo la edad
    $birthdate_dt = new DateTime($date); 
    $current_date_dt = new DateTime($current_date); 
    $age = $current_date_dt->diff($birthdate_dt)->y;


    // Verifico si la edad está dentro del rango deseado
    if ($age >= 90) {
      $errors[] = "La persona debe tener menos de 90 años para registrarse.";
    } elseif ($age < 18) {
      $errors[] = "La persona debe tener al menos 18 años para registrarse.";
    }
  }



  // Valido que se haya elegido al menos un día para el evento
  if (empty($_POST['day'])) {
    $errors[] = "Debes elegir al menos un día para asistir al evento.";
  }

  // Valido que se haya elegido un evento al que ir
  if (empty($_POST['event'])) {
    $errors[] = "Debes elegir un evento al que deseas asistir.";
  }


  // Si no hay errores, guardo los datos del formulario
  if (empty($errors)) {
    // Obtengo los datos del formulario
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $date = $_POST['date'];
    $email = $_POST['email'];
    $insta = $_POST['instagram'];
    $face = $_POST['facebook'];
    $tele = $_POST['telegram'];
    $profile = implode(', ', $_POST['profile']);
    $event = implode(', ', $_POST['event']);
    $day = implode(', ', $_POST['day']);

    // Creo una cadena con los datos a guardar en el archivo .txt
    $datos = "Name: $name \n Surname: $surname \n Birthdate: $date \n Email: $email \n Social-Media: $insta . $face . $tele \n Profile: $profile \n Event: $event \n Day in Event: $day \n";

    // Ruta del archivo .txt donde se guardarán los datos
    $ruta_archivo = __DIR__ . '/datos_evento.txt';

    // Escribo los datos en el archivo .txt y no los sobre escribo
    file_put_contents($ruta_archivo, $datos, FILE_APPEND);

    // dirigo al usuario a la página de exito para evitar el reenvío del formulario
    header("Location: success.html");
    exit;
  }
}

// Si hay errores, los muestro por pantalla
if (!empty($errors)) {
  foreach ($errors as $error) {
    echo "<p>$error</p>";
  }

  echo "<a href='index.html'>Volver al formulario</a>";

}
